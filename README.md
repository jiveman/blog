# Markdown Blog
Markdown Blog based on Charles Leifers PeeWee example blog: https://charlesleifer.com/blog/how-to-make-a-flask-blog-in-one-hour-or-less/

I upgraded to bootstrap4 & newer jquery, added tagging with a many-to-many relationship. Tagging input supports typeahead w/caching and ajax lookups.

## Requirements
- python3
- everything in the requirements.txt (python3-pip)


## Quickstart

Run the following commands to bootstrap your environment

```bash
    git clone https://gitlab.com/jiveman/blog
    mkvirtualenv blog
    cd blog
    setvirtualenvproject
    pip install -r requirements.txt
    ./manage.py # will launch a prompt based setup
    python app.py
```

You will see a blank blog. To create posts navigate to /login.



## Deployment

To deploy
```bash
 #TODO
 - uwsgi
 - letsencrypt
 - systemd service

```


## Shell

To open the interactive shell, run

```bash
    export FLASK_APP=app.py
    flask shell
```


