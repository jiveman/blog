$(document).ready(function(){


	var prefetch_tags = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.whitespace,
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		prefetch: {
			url: '/_get_tags?',
			cache: true
		},
		remote: {
			url: '/_get_tags/%QUERY',
			wildcard: '%QUERY'
		},
		limit: 10,
		minLength: 3
	});

	// var prefetch_countries = new Bloodhound({
	// 	datumTokenizer: Bloodhound.tokenizers.whitespace,
	// 	queryTokenizer: Bloodhound.tokenizers.whitespace,
	// 	local: ['python', 'perl'],
	// 	prefetch: {
	// 		url: 'https://raw.githubusercontent.com/twitter/typeahead.js/gh-pages/data/countries.json',
	// 		cache: true
	// 	}
	// });

	//prefetch_tags.clearPrefetchCache();
	prefetch_tags.initialize();

	var remote_tags = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		remote: {
			url: '/_get_tags/%QUERY',
			wildcard: '%QUERY'
		}
	});

	var remote_slugs = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		remote: {
			url: '/_get_slugs/%QUERY',
			wildcard: '%QUERY'
		}
	});

	// ** multiple data sources **	
	// $('.prefetch input').typeahead({
	//   hint: true,
	//   minLength: 1,
	//   highlight: true
	// },
	// {
	//   name: 'tags',
	//   source: prefetch_tags
	// },
	// {
	//   name: 'tags',
	//   source: remote_tags
	// }
	// );
	$('.prefetch input').typeahead({
	  hint: true,
	  minLength: 1,
	  highlight: true
	},
	{
	  name: 'tags',
      source: prefetch_tags
	});

	// $('.search #post_search').typeahead({
	//   hint: true,
	//   minLength: 3,
	//   highlight: true
	// },
	// {
	//   name: 'posts',
	//   source: remote_slugs
	// });
	



});
