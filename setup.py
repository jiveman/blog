#!/usr/bin/env python
import os
import sys
import json
import configparser
import ipdb

from loguru import logger
from flask import Flask
from flask_bcrypt import Bcrypt
import click

logger.remove()
logger.add(sys.stderr, colorize=True, format="[<cyan>{time:MM-DD HH:mm:ss}</cyan>] -  <level>{message}</level>", level="INFO")
logger.add(".blog_passwd.log", backtrace=True, format="{time:MM-DD HH:mm:ss}[{level}] {message}", rotation="0.5 MB")



bcrypt = Bcrypt()
BCRYPT_LOG_ROUNDS = 21
APP_DIR = os.path.dirname(os.path.realpath(__file__))

config = configparser.ConfigParser()
config_file = os.path.join(APP_DIR, 'app.cfg')
if (os.path.exists(config_file)):
    logger.info('loading conf {}'.format(config_file))
    config.read(config_file)
    if 'settings' not in config.sections():
        config['settings'] = {}
    if 'comments' not in config.sections():
        config['comments'] = {}
else:
    logger.info('creating new config file {}'.format(config_file))
    with open(config_file, 'w') as configfile:
        config['settings'] = {}
        config['comments'] = {}
        config.write(configfile)

@click.command()
@click.option('--password', prompt='Blog Password', hide_input=True,
              confirmation_prompt=True, help='Create new blog password')
@click.option('--secret_key', prompt='Blog Secret Key for encrypting sessions (random keystrokes)', hide_input=True,
              confirmation_prompt=False, help='Create new blog secret key for encrypting session data. (random keystrokes are fine)')
@click.option('--title', prompt='Blog Title', help='Blog Title', default=config['settings'].get('title', ''))
@click.option('--name', prompt='Full Name', help='Name as you want it to appear on the site', default=config['settings'].get('name', ''))
@click.option('--base_url', prompt='Site Base URL (e.g. https://acme.com)', help='Site Base URL e.g. https://acme.com', default=config['settings'].get('base_url', ''))
@click.option('--disqus_uri', prompt='Disqus URI', help='Full (comment platform) Disqus URI e.g. https://acme-com.disqus.com/embed.js or empty for none', default=config['settings'].get('disqus_uri', ''))
def setup(password, secret_key, title, name, base_url, disqus_uri):
    if not password:
        logger.error("<red>you must provide a password</red>")
    config['settings']['password'] = bcrypt.generate_password_hash(password).decode("utf-8")
    config['settings']['secret_key'] = bcrypt.generate_password_hash(password).decode("utf-8")
    config['settings']['title'] = title
    config['settings']['name'] = name
    config['settings']['base_url'] = base_url
    config['comments']['disqus_uri'] = disqus_uri
    logger.info('set {}'.format(config))

    if write_config(config_file, config):
        logger.info('<green>wrote {}</green>'.format(config_file))
    else:
        sys.exit(2)


def write_config(config_file, conf):
    try:
        with open(config_file, 'w') as out:
          conf.write(out)
    except Exception as err:
        logger.info("failed writing {}: {}".format(config_file, err))
        return False
    logger.opt(ansi = True).info('<green>wrote {}</green>'.format(config_file))
    # return True

if __name__ == '__main__':
    setup()

    # print "test"
